**# Task with arrays comparison**

There is an Country entity (with fields "id" and "name"). You have the first-version array of countries. You also have a second array with updated field values (except country), IDs was changed.

1) **Task:** get an array that contains only new country records.
**Forbidden:** to creating new array (except result)

2) **Task with star:** improve code so that execution speed will be linear (x=A). Hint: you must have only one array iteration