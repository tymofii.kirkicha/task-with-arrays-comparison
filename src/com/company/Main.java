package com.company;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Country> array1 = new ArrayList<>() {{
            add(new Country(1, "Ukraine", "380"));
            add(new Country(2, "France", "33"));
            add(new Country(3, "Germany", "49"));
        }};

        List<Country> array2 = new ArrayList<>() {{
            add(new Country(3, "Ukraine", "380 CHANGED"));
            add(new Country(4, "France", "33 CHANGED"));
            add(new Country(5, "Germany", "49"));
            add(new Country(1, "Egypt", "86"));
        }};

        List<Country> result;

        // Optional: for correct comparison in cases when size will be different
        if (array1.size() > array2.size()) {
            array1.removeAll(array2);
            result = array1;
        } else {
            array2.removeAll(array1);
            result = array2;
        }

        System.out.println(result);
    }
}

/*



 */