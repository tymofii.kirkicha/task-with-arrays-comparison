package com.company;

import java.util.Objects;

class Country {

    private Integer id;
    private String title;
    private String countryCode;

    public Country(Integer id, String title, String countryCode) {
        this.id = id;
        this.title = title;
        this.countryCode = countryCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Country country = (Country) o;
        return title.equals(country.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title);
    }

    @Override
    public String toString() {
        return "Country{" +
                "id=" + id +
                ", title='" + title + '\'' +
                '}';
    }
}